<?php
namespace App;
use App\Database\Connection;
use PDO;
class Helper extends Connection {

    static public function upload_img(){

        $file_name = $_FILES['image']['name'];
        $file_tmp = $_FILES['image']['tmp_name'];
        $fileName = substr(md5(time()),'0','8');

        $extName = end(explode('.',$file_name));

        $_POST['image'] = $fileName.'.'.$extName;

        move_uploaded_file($file_tmp,'uploads/'.$_POST['image']);

        return $_POST['image'];
    }

    public function delete_img($id){
        $stmt = $this->con->prepare("SELECT `image` FROM `add_doctor` WHERE `add_doctor`.`id` = :id");
        $stmt->execute(array(':id' => $id));
        $image_name = $stmt->fetch(PDO::FETCH_ASSOC);
        $dat = '../../assets/uploads/'.$image_name['image']; // uploads is a folder name or path
        if(isset($dat)){
            unlink($dat);
        }

    }

}