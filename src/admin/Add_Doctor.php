<?php
namespace App\admin;
use App\Database\Connection;
use PDOException;
use PDO;

class Add_Doctor extends Connection
{
    public $doctor_id;
    public $doctor_name;
    public $doctor_email;
    public $doctor_phone;
    public $doctor_category;
    public $doctor_address;
    private $image;

    public $id;

    public function set($data = array()){
        if(array_key_exists('doctor_id',$data)){
            $this->doctor_id = $data['doctor_id'];
        }
        if(array_key_exists('doctor_name',$data)){
            $this->doctor_name = $data['doctor_name'];
        }
        if(array_key_exists('doctor_email',$data)){
            $this->doctor_email = $data['doctor_email'];
        }
        if(array_key_exists('doctor_phone',$data)){
            $this->doctor_phone = $data['doctor_phone'];
        }
        if(array_key_exists('doctor_category',$data)){
            $this->doctor_category = $data['doctor_category'];
        }
        if(array_key_exists('doctor_address',$data)){
            $this->doctor_address = $data['doctor_address'];
        }
        if (array_key_exists('image', $data)) {
            $this->image = $data['image'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }
    public function store(){
        try {

            $stm =  $this->con->prepare("INSERT INTO `add_doctor`(`doctor_id`,`doctor_name`,`doctor_email`,`doctor_phone`,`doctor_category`,`doctor_address`,`unique_id`,`image`) 
                                        VALUES(:doctor_id,:doctor_name,:doctor_email,:doctor_phone,:doctor_category, :doctor_address, :image, :unique_id )");
            $result =$stm->execute(array(
                ':doctor_id' => $this->doctor_id,
                ':doctor_name' => $this->doctor_name,
                ':doctor_email' => $this->doctor_email,
                ':doctor_phone' => $this->doctor_phone,
                ':doctor_category' => $this->doctor_category,
                ':doctor_address' => $this->doctor_address,
                ':image' => $this->image,
                ':unique_id' => md5(time())

            ));
            if($result){
                $_SESSION['msg'] = 'Data successfully Added !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `add_doctor`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function view($id){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `add_doctor` WHERE unique_id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function update(){
        try{
            $stmt = $this->con->prepare("UPDATE `doctor_appointment`.`add_doctor` SET `doctor_id` = :doctor_id, `doctor_name` = :doctor_name, `doctor_email` = :doctor_email,`doctor_phone` = :doctor_phone,`doctor_category` = :doctor_category,`doctor_address` = :doctor_address WHERE `add_doctor`.`unique_id` = :id;");
            $stmt->bindValue(':doctor_id', $this->doctor_id, PDO::PARAM_INT);
            $stmt->bindValue(':doctor_name', $this->doctor_name, PDO::PARAM_INT);
            $stmt->bindValue(':doctor_email', $this->doctor_email, PDO::PARAM_INT);
            $stmt->bindValue(':doctor_phone', $this->doctor_phone, PDO::PARAM_INT);
            $stmt->bindValue(':doctor_category', $this->doctor_category, PDO::PARAM_INT);
            $stmt->bindValue(':doctor_address', $this->doctor_address, PDO::PARAM_INT);

            $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!';
                header('location:index.php');
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function delete($id){
        try{
            $stmt = $this->con->prepare("DELETE FROM `add_doctor` WHERE unique_id=:id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                $_SESSION['delete'] = 'Data successfully Deleted !!';
                header('location:index.php');
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }


}