<?php
namespace App\admin\Auth;
if(!isset($_SESSION)){
    session_start();
}
use App\Database\Connection;
use PDOException;
use PDO;

class Auth extends Connection
{
    public $patient_name;
    public $patient_email;
    public $patient_phone;
    public $patient_password;
    public $patient_address;
    public $id;



    public function set($data = array()){
        if(array_key_exists('patient_name',$data)){
            $this->patient_name = $data['patient_name'];
        }
        if(array_key_exists('patient_email',$data)){
            $this->patient_email = $data['patient_email'];
        }
        if(array_key_exists('patient_phone',$data)){
            $this->patient_phone = $data['patient_phone'];
        }
        if(array_key_exists('patient_password',$data)){
            $this->patient_password = $data['patient_password'];
        }
        if(array_key_exists('patient_address',$data)){
            $this->patient_address = $data['patient_address'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        return $this;
    }
    public function store(){
        try {

            $stm =  $this->con->prepare("INSERT INTO `patient_reg`(`patient_name`,`patient_email`,`patient_phone`,`patient_password`,`patient_address`, `unique_id`) 
                                        VALUES(:patient_name,:patient_email,:patient_phone,:patient_password,:patient_address,:unique_id )");
            $result =$stm->execute(array(
                ':patient_name' => $this->patient_name,
                ':patient_email' => $this->patient_email,
                ':patient_phone' => $this->patient_phone,
                ':patient_password' => $this->patient_password,
                 ':patient_address' => $this->patient_address,
                ':unique_id' => md5(time())

            ));
            if($result){
                $_SESSION['msg'] = 'Data successfully Registered !!!';
                header('location:patient_login.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `patient_reg`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function view($id){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `patient_reg` WHERE unique_id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function login(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `patient_reg` WHERE (patient_name = :patient_name OR patient_email = :patient_email) AND patient_password = :patient_password");
            $stm->bindValue(':patient_name', $this->patient_name, PDO::PARAM_STR);
            $stm->bindValue(':patient_email', $this->patient_email, PDO::PARAM_STR);
            $stm->bindValue(':patient_password', $this->patient_password, PDO::PARAM_STR);
            $stm->execute();
            if($stm->rowCount()>0){
                $_SESSION['patient_name'] = 'admin';
                header('location:../patient/patient_profile.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }











}