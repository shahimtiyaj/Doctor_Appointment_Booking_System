<?php
session_start();
include_once '../../../vendor/autoload.php';
if(isset($_POST['submit'])){

    $auth = new \App\admin\Auth\Auth1();
   /* $_POST['email'] = $_POST['user_email'];
    $_POST['user'] = $_POST['user_email'];*/
    $data = $auth->set($_POST)->login();
   // var_dump($data);
}

?>



<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<base href="http://localhost/Doctor%20Appointment/view/admin/Auth/login.php"/>

    <!--<base href="http://localhost/Doctor%20Appointment/" />-->

    <!-- flexslider-css-file -->
    <link rel="stylesheet" href="../../../assets/front/css/flexslider.css" type="text/css" media="screen" property="" />
    <!-- //flexslider-css-file -->

    <link rel="stylesheet" href="../../../assets/front/css/easy-responsive-tabs.css">

    <link href="../../../assets/front/css/font-awesome.css" rel="stylesheet"><!-- font-awesome icons-css-file -->
    <link href="../../../assets/front/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">	<!-- bootstrap-css-file -->
    <link href="../../../assets/front/css/style.css" type="text/css" rel="stylesheet" media="all">	<!-- style-css-file -->


    <!-- gallery -->
    <link rel="stylesheet" href="../../../assets/front/css/lightbox.css">
    <!-- //gallery -->

    <!-- fonts -->
    <link href="//fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Cinzel:400,700,900" rel="stylesheet">
    <!-- //fonts -->

    <!-- Default-JavaScript-File -->
    <script type="text/javascript" src="../../../assets/front/js/jquery-2.1.4.min.js"></script>
    <!-- //Default-JavaScript-File -->

    <script src="../../../assets/front/js/bootstrap.js"></script>	<!-- //bootstrap-JavaScript-File -->
</head>

<body>

<div id="container">
    <div class="row">
        <div class="col-lg-4 col-md-offset-4"">
            <h1 class="page-header">Admin Login</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

<div class="row">
    <div class="col-lg-12">

        <div class="panel-body">
            <div class="row">
                <div class="col-lg-4 col-md-offset-4">
                    <form role="form" action="" method="post" enctype="multipart/form-data" >

                        <div class="form-group">
                            <label>User Name</label>
                            <input name="user" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input  type="password" name="password" class="form-control">
                        </div>

                        <button type="submit" name="submit" class="btn btn-info">Login</button>
                        New User! <a href="registration.php" onClick=""> Sign Up Here </a>
                    </form>
                </div>
            </div>
            <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->co
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

    <!-- jQuery -->
    <script src="../../../assets/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../../../assets/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../../../assets/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../../../assets/dist/js/sb-admin-2.js"></script>

</body>

</html>
