<?php
include_once('../include/header.php');
include_once '../../../vendor/autoload.php';

?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Doctor</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-2">
                                <form role="form" action="../../../view/admin/admin/store.php" method="post" enctype="multipart/form-data" >
                                    <div class="form-group">
                                        <label>Doctor ID</label>
                                        <input name="doctor_id" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Doctor Name</label>
                                        <input name="doctor_name" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label >Email </label>
                                        <input type="email" name="doctor_email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label >Phone </label>
                                        <input type="number" name="doctor_phone" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select name="doctor_category" class="form-control">
                                            <option>Select One</option>
                                            <option>Heart</option>
                                            <option>Bone</option>
                                            <option>Surgery</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Adress</label>
                                        <textarea name="doctor_address" class="form-control" rows="3"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Upload Image</label>
                                        <input type="file" name="image" class="form-control"/>
                                    </div>
                                    <button type="submit" class="btn btn-info">Submit</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->co
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->


<?php
include_once '../include/footer.php';
?>