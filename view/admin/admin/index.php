<?php
include_once('../include/header.php');
include_once '../../../vendor/autoload.php';

$doctor_list = new \App\admin\Add_Doctor();
$doctor_list = $doctor_list->index();
?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">All Doctors List</h1>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>SL NO</th>
                        <th>Doctor ID</th>
                        <th>Doctor Name</th>
                        <th>Doctor E-mail</th>
                        <th>Doctor Phone</th>
                        <th>Doctor Category</th>
                        <th>Doctor Address</th>
                        <th>Doctor Image</th>

                        <th>Action</th>
                    </tr>
                    </thead>

                <?php

                $sl = 1;
                foreach ($doctor_list as $doctor){
                    ?>

                    <tbody>
                    <tr>
                        <th scope="row"><?php echo $sl++ ?></th>
                        <td><?php echo $doctor['doctor_id']?></td>
                        <td><?php echo $doctor['doctor_name']?></td>
                        <td><?php echo $doctor['doctor_email']?></td>
                        <td><?php echo $doctor['doctor_phone']?></td>
                        <td><?php echo $doctor['doctor_category']?></td>
                        <td><?php echo $doctor['doctor_address']?></td>

                        <td class="text-center">
                            <img src="uploads/<?php echo $doctor['image']?>" width="100" alt="" >
                        </td>

                        <td class="center">
                            <a class="text-info" href="view.php?id=<?php echo $doctor['unique_id'] ?>">View</a> |
                            <a class="text-info" href="edit.php?id=<?php echo $doctor['unique_id'] ?>">Edit</a> |
                            <a class="text-info" href="delete.php?id=<?php echo $doctor['unique_id'] ?>">Delete</a>

                        </td>
                    </tr>

                    <?php }
                    ?>

                    </tbody>
                </table>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>