<?php
include_once('../include/header.php');
include_once '../../../vendor/autoload.php';

$doctor = new \App\admin\Add_Doctor();
$doctor = $doctor->view($_GET['id']);
?>
    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Doctor Details</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="col-lg-12">

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Doctor ID</th>
                    <th>Doctor Name</th>
                    <th>Doctor E-mail</th>
                    <th>Doctor Phone</th>
                    <th>Doctor Category</th>
                    <th>Doctor Address</th>

                </tr>
                </thead>

                <tbody>
                <tr>

                    <td><?php echo $doctor['doctor_id']?></td>
                    <td><?php echo $doctor['doctor_name']?></td>
                    <td><?php echo $doctor['doctor_email']?></td>
                    <td><?php echo $doctor['doctor_phone']?></td>
                    <td><?php echo $doctor['doctor_category']?></td>
                    <td><?php echo $doctor['doctor_address']?></td>


                </tr>

                </tbody>
            </table>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>