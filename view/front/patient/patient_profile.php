<?php
include_once '../include/header_after_patient_login.php';
include_once '../../../vendor/autoload.php';

//$patient = new \App\admin\Auth\Auth();

//$patient = $patient->view($_GET['id']);

//var_dump($patients);

?>

<!-- about -->

<div class="about" id="about">
		<div class="container">
			<div class="w3ls-heading">
				<h3>My Profile</h3>
			</div>

				<div class="col-md-12 about-left">
				
				<div class="row">
                <div class="col-lg-4 col-md-offset-4">
				<form>
				<div class="form-group">
					<label for="user_id">User Id </label>
					<input type="text" class="form-control" value="" id="user_id" aria-describedby="nameHelp" placeholder="User Id">
				  </div>
				 <div class="form-group">
					<label for="name">Name </label>
					<input type="text" class="form-control" value="" id="name" aria-describedby="nameHelp" placeholder="Name">
				  </div>
				  <div class="form-group">
					<label for="email">Email </label>
					<input type="email" class="form-control" value="" id="email" aria-describedby="emailHelp" placeholder="E-mail">
				  </div>
				   <div class="form-group">
					<label for="phone">Phone </label>
					<input type="number" class="form-control"  value ="" id="phone" aria-describedby="phonelHelp" placeholder="Phone">
				  </div>
				  <div class="form-group">
					<label for="address">Address</label>
					<input type="text" class="form-control"  value="" id="address" placeholder="Address">
				  </div>
				  <div class="form-group">
                      <label >Update Image</label>
                      <input type="file" name="image"/>
                       <img width="10" src="../../../assets/uploads/image.jpg" alt="">
                   </div>
					
				  <button type="submit" class="btn btn-primary">Update</button>

				</form>
				</div>
				</div>
				
				</div>
			
					
		</div>
		
	</div>

	
<!-- //about -->

	
<!-- contact -->

<!-- //contact -->
<?php
include_once '../include/footer.php';
?>


