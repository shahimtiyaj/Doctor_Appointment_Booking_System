<?php
include_once '../include/header_after_patient_login.php';
?>


<!-- about -->

<div class="about" id="about">
    <div class="container">
        <div class="w3ls-heading">
            <h3>Treatment History</h3>
        </div>

        <div class="col-md-12 about-left">



            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>SL NO</th>
                    <th>User ID</th>
                    <th>Diseases</th>
                    <th>Treatment</th>
                    <th>Diseases Note</th>
                    <th>Date & Time</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>1001</td>
                    <td>Pills</td>
                    <td>Normal</td>
                    <td>11.30</td>


                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>Jacob</td>
                    <td>1002</td>
                    <td>Pills</td>
                    <td>Normal</td>
                    <td>11.30</td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td>Jacob</td>
                    <td>1003</td>
                    <td>Pills</td>
                    <td>Normal</td>
                    <td>11.30</td>
                </tr>
                </tbody>
            </table>



        </div>


    </div>

</div>


<!-- //about -->


<!-- contact -->

<!-- //contact -->

<?php
include_once '../include/footer.php';
?>


