<?php
include_once '../include/header_after_patient_login.php';
?>


<!-- about -->

<div class="about" id="about">
		<div class="container">
			<div class="w3ls-heading">
				<h3>Booking History</h3>
			</div>
			
		
				<div class="col-md-12 about-left">
				
				
				
					<table class="table table-bordered">
						  <thead>
							<tr>
							  <th>SL NO</th>
							  <th>Appointment ID</th>
							  <th>User ID</th>
							  <th>Doctor Name</th>
							  <th>Date</th>
							  <th>Time</th>
							   <th>Action</th>
							</tr>
						  </thead>
						  <tbody>
							<tr>
							  <th scope="row">1</th>
							  <td>Mark</td>
							  <td>Otto</td>
							  <td>@mdo</td>
							  <td>12-05-17</td>
							  <td>11.30</td>
							  <td class ="center">
							   <a class="text-danger" href="#">Cancel</a>
							  </td>


							  
							</tr>
							<tr>
							  <th scope="row">2</th>
							  <td>Jacob</td>
							  <td>Thornton</td>
							  <td>@fat</td>
						       <td>12-05-17</td>
							  <td>11.30</td>
							   <td class ="center">
							   <a class="text-danger" href="#">Cancel</a>
							  </td>
							</tr>
								<tr>
							  <th scope="row">3</th>
							  <td>Jacob</td>
							  <td>Thornton</td>
							  <td>@fat</td>
						      <td>12-05-17</td>
							  <td>11.30</td>
							  <td class ="center">
							   <a class="text-danger" href="#">Cancel</a>
							  </td>
							</tr>
						  </tbody>
						</table>
										
				
				
				</div>
			
					
		</div>
		
	</div>

	
<!-- //about -->

	
<!-- contact -->

<!-- //contact -->

<?php
include_once '../include/footer.php';
?>


