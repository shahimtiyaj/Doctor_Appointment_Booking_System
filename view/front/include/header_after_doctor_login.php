<?php
if(!isset($_SESSION)){
    session_start();
}
?>


<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<head>
    <title>Doctor Appointment</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content="Medical Care a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

    <!--<base href="http://localhost/Doctor%20Appointment/" />-->

    <!-- flexslider-css-file -->
    <link rel="stylesheet" href="../../../assets/front/css/flexslider.css" type="text/css" media="screen" property="" />
    <!-- //flexslider-css-file -->

    <link rel="stylesheet" href="../../../assets/front/css/easy-responsive-tabs.css">

    <link href="../../../assets/front/css/font-awesome.css" rel="stylesheet"><!-- font-awesome icons-css-file -->
    <link href="../../../assets/front/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">	<!-- bootstrap-css-file -->
    <link href="../../../assets/front/css/style.css" type="text/css" rel="stylesheet" media="all">	<!-- style-css-file -->


    <!-- gallery -->
    <link rel="stylesheet" href="../../../assets/front/css/lightbox.css">
    <!-- //gallery -->

    <!-- fonts -->
    <link href="//fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Cinzel:400,700,900" rel="stylesheet">
    <!-- //fonts -->

    <!-- Default-JavaScript-File -->
    <script type="text/javascript" src="../../../assets/front/js/jquery-2.1.4.min.js"></script>
    <!-- //Default-JavaScript-File -->

    <script src="../../../assets/front/js/bootstrap.js"></script>	<!-- //bootstrap-JavaScript-File -->

</head>
<!-- Head -->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- banner -->
<div id="home" class="w3ls-banner">
    <!-- header -->
    <div class="header-w3layouts">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">

                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Doctor Appointment</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- <h1><a class="navbar-brand" href="../../../index.php">Doctor Appointment</a></h1>-->
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav navbar-right cl-effect-15">
                        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                        <li class="hidden"><a class="page-scroll" href="#page-top"></a>	</li>
                        <li><a href="../../front/doctor/my_appointment.php">My Appointments</a></li>
                        <li><a href="../doctor/view_patient.php">View Patient</a></li>
                        <li><a href="../../front/doctor/doctor_profile.php">My Profile</a></li>
                        <li><a href="../doctor/add_description.php">Add Description</a></li>
                        <!--<li><a href="../../front/patient/cencel_book.html">Cancel Booking</a></li>-->
                        <li><a href="../../../index.php">Logout</a></li>

                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>
    </div>
    <!-- //header -->
    <!--banner-->
    <!--Slider-->
    <div class="w3l_banner_info">
        <div class="col-md-5 banner-form-agileinfo">
            <img src="../../../assets/front/images/alt1.png" alt="" />
        </div>
        <div class="col-md-7 slider">
            <div class="callbacks_container">
                <ul class="rslides" id="slider3">
                    <li>
                        <div class="slider_banner_info">
                            <h4>WE make healthy</h4>
                            <p>Medical school education and post graduate education empha -size thoroughness.The good physician treats the disease, great physician treats the patient who has the disease.</p>
                        </div>

                    </li>
                    <li>
                        <div class="slider_banner_info">
                            <h4>Medicine is a science</h4>
                            <p>The best doctor is the one you run to and can't find.We should be concerned not only about the health of individual patients, but also the health of our entire society.</p>
                        </div>

                    </li>
                    <li>
                        <div class="slider_banner_info">
                            <h4>Nothing cures health</h4>
                            <p>Never go to a doctor whose office plants have died.You know what they call the fellow who finishes last in his medical school graduating class? They call him 'Doctor. </p>
                        </div>

                    </li>
                    <li>
                        <div class="slider_banner_info">
                            <h4>We do best treatment</h4>
                            <p>Time is generally the best doctor.so well choose your best doctor before time lossesThe art of medicine consists in amusing the patient while nature cures the disease.</p>
                        </div>

                    </li>
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
        <!--//Slider-->

    </div>
    <!--//banner-->

</div>
<!-- //banner -->
