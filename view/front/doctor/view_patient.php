<?php
include_once '../include/header_after_doctor_login.php';
include_once '../../../vendor/autoload.php';
$patient = new \App\admin\Auth\Auth();

$patients = $patient->index();
?>


<!-- about -->

<div class="about" id="about">
    <div class="container">
        <div class="w3ls-heading">
            <h3>Patient Details</h3>
        </div>

        <div class="col-md-12 about-left">



            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>SL NO</th>
                    <th>User ID</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Mobile</th>
                    <th>E-mail</th>
                    <th>Action</th>


                </tr>
                </thead>
                <tbody>

                <?php
                $all_patient = new \App\admin\Auth\Auth();
                $patients = $all_patient->index();
                $sl = 1;
                foreach ($patients as $patient){
                ?>
                <tr>
                    <th scope="row"> <?php echo $sl++?> </th>
                    <td>1001</td>
                    <td><?php echo $patient['patient_name']?></td>
                    <td><?php echo $patient['patient_email']?></td>
                    <td><?php echo $patient['patient_phone']?></td>
                    <td><?php echo $patient['patient_address']?></td>

                    <td class="center">
                        <a class="text-info" href="patient_treatment_history.php?id=<?php echo $patient['unique_id'] ?>">View</a> |
                    </td>

                </tr>
                <?php } ?>
                </tbody>
            </table>

        </div>


    </div>
</div>


<!-- //about -->


<!-- contact -->

<!-- //contact -->

<?php
include_once '../include/footer.php';
?>

