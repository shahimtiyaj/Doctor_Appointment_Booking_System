<?php
include_once '../include/header_after_doctor_login.php';
include_once '../../../vendor/autoload.php';

$patient = new \App\admin\Auth\Auth();

$patient = $patient->view($_GET['id']);

//var_dump($patient);

?>


<!-- about -->

<div class="about" id="about">
    <div class="container">
        <div class="w3ls-heading">
            <h3>Patient Details</h3>
        </div>
        <div class="col-md-12 about-left">

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>SL NO</th>
                    <th>User ID</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Mobile</th>
                    <th>E-mail</th>
                    <th>Sex</th>
                    <th>Age</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>1001</td>
                    <td><?php echo $patient['patient_name']?></td>
                    <td><?php echo $patient['patient_address']?></td>
                    <td><?php echo $patient['patient_phone']?></td>
                    <td><?php echo $patient['patient_email']?></td>
                    <td>Male</td>
                    <td>24</td>
                </tr>
                </tbody>
            </table>



        </div>

    </div>
</div>

<!-- //about -->


<!-- contact -->

<!-- //contact -->

<?php
include_once '../include/footer.php';
?>


