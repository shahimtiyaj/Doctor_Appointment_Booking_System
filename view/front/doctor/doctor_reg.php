<?php
include_once '../../../vendor/autoload.php';
if(isset($_POST['submit'])){
    $auth = new \App\admin\Auth\Auth();
    $auth->set($_POST)->store();
}
include_once '../include/header.php';
?>


<!-- about -->

<div class="about" id="about">
    <div class="container">
        <div class="w3ls-heading">
            <h3>Sign Up</h3>
        </div>
        <div class="col-md-12 about-left">
            <form role="form" action="" method="post">
                <div class="form-group">
                    <label for="name">Name </label>
                    <input type="text" name="patient_name" class="form-control" id="name" aria-describedby="nameHelp" placeholder="Name">
                </div>
                <div class="form-group">
                    <label for="email">Email </label>
                    <input type="email" name="patient_email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="E-mail">
                </div>
                <div class="form-group">
                    <label for="phone">Phone </label>
                    <input type="number" name="patient_phone" class="form-control" id="phone" aria-describedby="phonelHelp" placeholder="Phone">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="patient_password" class="form-control" id="password" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="password">Confirm Password</label>
                    <input type="password" name="patient_confirm_password" class="form-control" id="password" placeholder="Confirm Password">
                </div>
                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" name="patient_address" class="form-control" id="address" placeholder="Address">
                </div>
                <div class="form-group">
                    <label for="image">Upload Image</label>
                    <input type="file" name="patient_image" class="form-control-file" id="image">
                </div>

                <button type="submit" name="submit" class="btn btn-primary">Submit</button>

                <!--  <a href="patient_login.php" class="btn btn-primary" role="button">Submit</a>-->

                If you have already register! please <a href="doctor_login.php" onClick=""> Login Here </a>

            </form>
        </div>

    </div>
</div>


<!-- //about -->


<!-- contact -->

<div class="line">
</div>
<div class="contact" id="contact">
    <div class="container">
        <div class="w3ls-heading">
            <h3>Contact</h3>
        </div>
        <div class="contact-top-grids">
            <div class="col-md-6 contact-left-">
                <div class="left-top">
                    <h4>Contact information</h4>
                    <p><i class="fa fa-calendar" aria-hidden="true"></i><strong>Monday - Friday <span class="dot">:</span></strong>9:30 AM to 6:30 PM</p>
                    <p><i class="fa fa-map-marker" aria-hidden="true"></i><strong class="p">address <span class="dot1">:</span></strong>Chicago, 5th main road, carnivalle</p>
                    <p><i class="fa fa-phone" aria-hidden="true"></i><strong class="p1">phone <span class="dot2">:</span></strong>+040 2345 6789</p>
                    <p><i class="fa fa-envelope" aria-hidden="true"></i><strong class="p2">email <span class="dot3">:</span></strong><a href="mailto:info@example.com">example@email.com</a></p>
                </div>
                <div class="left-bottom">
                    <h4>get connected</h4>
                    <p>Pellentesque finibus dapibus volutpat. Curabitur imperdiet vulputate rhoncus. Nullam
                        scelerisque magna non turpis euismod bibendum.</p>
                    <div class="right-w3l fotw3">
                        <ul class="top-links">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="col-md-6 contact-right">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2970.296948478801!2d-87.70494908450506!3d41.88647047922158!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e32a54d21f20f%3A0x51b98b6ffbdca819!2sW+Fulton+St%2C+Chicago%2C+IL%2C+USA!5e0!3m2!1sen!2sin!4v1489574334335"></iframe>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="contact-w3ls">
            <form action="#" method="post">
                <div class="col-md-7 col-sm-7 contact-left agileits-w3layouts">
                    <input type="text" name="First Name" placeholder="Name" required="">
                    <input type="email"  class="email" name="Email" placeholder="Email" required="">
                    <input type="text" name="Number" placeholder="Mobile Number" required="">
                    <!-- <input type="text" class="email" name="Last Name" placeholder="Last Name" required=""> -->
                </div>
                <div class="col-md-5 col-sm-5 contact-right agileits-w3layouts">
                    <textarea name="Message" placeholder="Message" required=""></textarea>
                    <input type="submit" value="Submit">
                </div>
                <div class="clearfix"> </div>
            </form>
        </div>

    </div>
</div>
<!-- //contact -->
<?php
include_once '../include/footer.php';
?>


